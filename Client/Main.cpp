#include <iostream> 
#include <fstream>
#include "ObjClient.h"


using namespace std;

void main(int argc, char* argv[]) {
	// check parameters
	if (argc != 3) {
		cout << "Parameters number is wrong!" << endl
			<< "Usage: Server.exe <server hostname> <server port>" << endl;
		return;
	}

	// connect to server and get Obj data
	cout << "Trying to connect to the server " << argv[1] << ":" << argv[2] << endl;
	ObjClient* client = new ObjClient(argv[1], argv[2]);
	if (!client->connect())
		return;

	cout << "Requesting for obj file data..." << endl;
	shared_ptr<Geometry> g = client->getObj();
	if (!g)
		return;

	// shaw data part
	cout << "Data (partly):" << endl;
	string data = g->toString();
	if (data.length() > 1000)
		cout << data.substr(0, 1000) << "..." << endl << "Whole data is placed to './data.obj' file." << endl;

	// save Obj file
	ofstream myfile;
	string fileName = "./data.obj";
	myfile.open(fileName);
	if (!myfile.is_open()) {
		cout << "Can't open file to save: '" << fileName << "'." << endl;
		return;
	}
	myfile << g->toString();
	myfile.close();

	// get obj face elements partly
	cout << "Requesting for obj file part data..." << endl;
	const int startElement = 1;
	const int numElements = 500;
	shared_ptr<Geometry> gPart =  client->getObjPart(startElement, numElements);
	if (!gPart)
		return;

	// shaw faces data part
	cout << "Faces data (partly):" << endl;
	data = gPart->toString();
	if (data.length() > 1000)
		cout << data.substr(0, 1000) << "..." << endl << "Whole data is placed to './data_part.obj' file." << endl;

	// save Obj file
	fileName = "./data_part.obj";
	myfile.open(fileName);
	if (!myfile.is_open()) {
		cout << "Can't open file to save: '" << fileName << "'." << endl;
		return;
	}
	myfile << gPart->toString();
	myfile.close();

	// disconnect from server
	client->disconnect();
}
