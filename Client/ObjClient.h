#ifndef HH_OBJ_CLIENT_
#define HH_OBJ_CLIENT_

#include<winsock2.h>
#include <ws2tcpip.h>
#include <string>

#include "Geometry.h"

using namespace std;


class ObjClient {
public:
	ObjClient(string host, string port) : _host(host), _port(port) {};
	bool connect();
	bool disconnect();
	shared_ptr<Geometry> getObj();
	shared_ptr<Geometry> getObjPart(int start, int count);
	
private:
	string _host;
	string _port;
	SOCKET _clientSocket;	
};


string to_binary(const string str);
string from_binary(const string binary);

#endif // HH_OBJ_CLIENT_