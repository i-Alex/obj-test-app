#include "ObjClient.h"
#include "ObjFileLoader.h"
//#include "GeometryManager.h"

#include <iostream>
#include <sstream>
#include <bitset>

using namespace std;


bool ObjClient::connect() {
	WSADATA wsaData;
	int result;
	
	// init Winsock v2.2
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0) {
		cout << "WSAStartup failed with error:" << result << endl;
		return false;
	}

	struct addrinfo hints;
	struct addrinfo *resultAddr = nullptr;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	//hints.ai_flags = AI_PASSIVE;

	// resolve the server address:port
	result = getaddrinfo(_host.c_str(), _port.c_str(), &hints, &resultAddr);
	if (result != 0) {
		cout << "Failed to get address info for host " << _host << " and port " << _port << ". Error " << result << "occurred." << endl;
		WSACleanup();
		return false;
	}

	// create client socket
	_clientSocket = INVALID_SOCKET;
	_clientSocket = socket(resultAddr->ai_family, resultAddr->ai_socktype, resultAddr->ai_protocol);
	if (_clientSocket == INVALID_SOCKET) {
		cout << "Socket failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(resultAddr);
		WSACleanup();
		return false;
	}

	// connect to server
	result = ::connect(_clientSocket, (sockaddr *)resultAddr->ai_addr, (int)resultAddr->ai_addrlen);
	if (result == SOCKET_ERROR) {
		cout << "Connection failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(resultAddr);
		closesocket(_clientSocket);
		WSACleanup();
		return false;
	}
	freeaddrinfo(resultAddr);

	cout << "Connected successfully." << endl;
	return true;
}


bool ObjClient::disconnect() {
	if (_clientSocket == SOCKET_ERROR) {
		cout << "Client already disconnected." << endl;
		return false;
	}
	string requestMessage = to_binary("CLOSE");
	send(_clientSocket, requestMessage.c_str(), requestMessage.length(), 0);

	closesocket(_clientSocket);
	cout << "Disconnected successfully." << endl;
	return true;
}


shared_ptr<Geometry> ObjClient::getObj() {
	string requestMessage = to_binary("GET_OBJ");
	int result = send(_clientSocket, requestMessage.c_str(), requestMessage.length(), 0);
	if (result == SOCKET_ERROR) {
		cout << "Can't send request data." << endl;
		closesocket(_clientSocket);
		return nullptr;
	}

	string objData;

	// set retrieve data timeout
	DWORD timeout = 2 * 1000;
	setsockopt(_clientSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	const int bufferLen = 10000;
	char buffer[bufferLen];
	do {
		result = recv(_clientSocket, buffer, bufferLen, 0);
		if (result > 0) {
			if (result < bufferLen)
				buffer[result] = '\0';
			objData += from_binary(buffer); 
		}
	} while (result > 0 || objData.length() == 0);


	cout << "Obj data retrieved." << endl;

	shared_ptr<Geometry> g(new Geometry());
	istringstream s(objData);
	if (!ObjFileLoader::loadObj(g, s))
		return nullptr;
	return g;
}


shared_ptr<Geometry> ObjClient::getObjPart(int start, int count) {
	if (start < 1 || count < 1) {
		cout << "Can't get obj part! Parameters values are wrong!" << endl;
		return nullptr;
	}

	string requestMessage = "GET_OBJ_PART " + to_string(start) + " " + to_string(count);
	requestMessage = to_binary(requestMessage);
	int result = send(_clientSocket, requestMessage.c_str(), requestMessage.length(), 0);
	if (result == SOCKET_ERROR) {
		cout << "Can't send request data." << endl;
		closesocket(_clientSocket);
		return nullptr;
	}

	string objData;

	// set retrieve data timeout
	DWORD timeout = 2 * 1000;
	setsockopt(_clientSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	const int bufferLen = 10000;
	char buffer[bufferLen];
	do {
		result = recv(_clientSocket, buffer, bufferLen, 0);
		if (result > 0) {
			if (result < bufferLen)
				buffer[result] = '\0';
			objData += from_binary(buffer);
		}
	} while (result > 0 || objData.length() == 0);

	if (objData.compare(0, 6, "Error:") == 0) {
		cout << objData << endl;
		return nullptr;
	}

	cout << "Obj faces part data retrieved." << endl;
	shared_ptr<Geometry> g(new Geometry());
	istringstream s(objData);
	if (!ObjFileLoader::loadObj(g, s))
		return nullptr;
	return g;
}

string to_binary(const string str) {
	std::ostringstream oss;
	for (auto c : str) {
		oss << bitset<8>(c);
	}
	return oss.str();
}

string from_binary(const string binary) {
	stringstream ss(binary);
	string res = "";
	bitset<8> tmp;
	while (ss >> tmp)
		res += static_cast<char>(tmp.to_ulong());
	return res;
}