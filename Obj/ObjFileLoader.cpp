#include "ObjFileLoader.h"
#include "GeometryElements.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

using namespace std;

bool ObjFileLoader::loadObj(shared_ptr<Geometry> geometry, string fileName) {
	if (!geometry)
		return false;

	if (fileName.length() < 4 || fileName.compare(fileName.length() - 4, 4, ".obj") != 0) {
		cout << "File type is wrong! Use '.obj' type instead." << endl;
		return false;
	}


	std::ifstream infile(fileName);
	if (!infile) {
		cout << "File with name '" + fileName + "' is missed!" << endl;
		return false;
	}

	return loadObj(geometry, infile);
}


bool ObjFileLoader::loadObj(shared_ptr<Geometry> geometry, istream& stream) {
	shared_ptr<GeometryObject> currentObject;
	shared_ptr<GeometryGroup> currentGroup;

	// parse stream
	for (string line; getline(stream, line);) {
		if (line.length() == 0 || line[0] == '#') // ignore empty and comment lines
			continue;

		std::istringstream lineStream(line);
		string element, tmp;
		lineStream >> element;

		if (element.compare("mtllib") == 0) { // material library
			lineStream >> tmp;
			geometry->setMtllib(tmp);
		}
		else if (element.compare("o") == 0) { // object
			currentObject = parseObject(line);
			geometry->appendObject(currentObject);
		}
		else if (element.compare("g") == 0) { // group
			currentGroup = parseGroup(line);

			if (!currentObject) { // file contains no object definition
				currentObject = shared_ptr<GeometryObject>(new GeometryObject());
				geometry->appendObject(currentObject);
			}
			currentObject->appendGroup(currentGroup);
		}
		else { // other elements
			if (!currentObject) { // file starts with no object definition
				currentObject = shared_ptr<GeometryObject>(new GeometryObject());
				geometry->appendObject(currentObject);
			}
			if (!currentGroup) { // file starts with no group definition
				currentGroup = shared_ptr<GeometryGroup>(new GeometryGroup());
				currentObject->appendGroup(currentGroup);
			}

			shared_ptr<GeometryBaseElement> be;
			if (element.compare("v") == 0) // verticle
				be = parseVerticle(line);
			else if (element.compare("vt") == 0) // texture coordinates
				be = parseVerticleTexture(line);
			else if (element.compare("vn") == 0) // vertex normals
				be = parseVertexNormal(line);
			else if (element.compare("vp") == 0) // parameter space vertices
				be = parseParameterSpaceVerticle(line);
			else if (element.compare("f") == 0) // polygonal face element
				be = parsePolygonalFace(line);
			else if (element.compare("usemtl") == 0) // material name
				be = parseMtlElement(line);
			else if (element.compare("s") == 0) // smooth shading
				be = parseSmooth(line);

			if (!be) {
				cout << "Can't parse line: '" + line + "'. Syntax error occurred!" << endl;
				return false;
			}
			else
				currentGroup->appendElement(be);
		}
	}

	return true;
}


shared_ptr<GeometryObject> ObjFileLoader::parseObject(string line) {
	// get object name
	string name = "";
	int paramStrLen = 2;
	if (line.length() > paramStrLen) {
		name = line.substr(paramStrLen, line.length() - paramStrLen);
		return shared_ptr<GeometryObject>(new GeometryObject(name));
	}
	else {
		return shared_ptr<GeometryObject>(new GeometryObject());
	}
}

shared_ptr<GeometryGroup> ObjFileLoader::parseGroup(string line) {
	// get group name
	string name = "";
	int paramStrLen = 2;
	if (line.length() > paramStrLen) {
		name = line.substr(paramStrLen, line.length() - paramStrLen);
		return shared_ptr<GeometryGroup>(new GeometryGroup(name));
	}
	else {
		return shared_ptr<GeometryGroup>(new GeometryGroup());
	}
}

shared_ptr<GeometryVerticle> ObjFileLoader::parseVerticle(string line) {
	double coords[4] = { 0.0, 0.0, 0.0, 1.0 }; // x, y, z[, w]
	string::size_type sz;
	line = line.substr(2);
	try {
		coords[0] = stod(line, &sz);
		line = line.substr(sz);
		coords[1] = stod(line, &sz);
		line = line.substr(sz);
		coords[2] = stod(line, &sz);
		line = line.substr(sz);
	}
	catch (...) {
		return nullptr;
	}
	try { // last coordinate may be absent
		coords[3] = stod(line, &sz);
	}
	catch (...) { }

	return shared_ptr<GeometryVerticle>(new GeometryVerticle(coords[0], coords[1], coords[2], coords[3]));
}

shared_ptr<GeometryVerticleTexture> ObjFileLoader::parseVerticleTexture(string line) {
	double coords[3] = { 0.0, 0.0, 0.0 }; // u, v[, w]
	string::size_type sz;
	line = line.substr(3);
	try {
		coords[0] = stod(line, &sz);
		line = line.substr(sz);
		coords[1] = stod(line, &sz);
		line = line.substr(sz);
	}
	catch (...) {
		return nullptr;
	}
	try { // last coordinate may be absent
		coords[2] = stod(line, &sz);
	}
	catch (...) {};
	return shared_ptr<GeometryVerticleTexture>(new GeometryVerticleTexture(coords[0], coords[1], coords[2]));
}

shared_ptr<GeometryVertexNormal> ObjFileLoader::parseVertexNormal(string line) {
	double coords[3] = { 0.0, 0.0, 0.0 }; // x, y, z
	string::size_type sz;
	line = line.substr(3);
	try {
		coords[0] = stod(line, &sz);
		line = line.substr(sz);
		coords[1] = stod(line, &sz);
		line = line.substr(sz);
		coords[2] = stod(line, &sz);
	}
	catch (...) {
		return nullptr;
	}

	return shared_ptr<GeometryVertexNormal>(new GeometryVertexNormal(coords[0], coords[1], coords[2]));
}


shared_ptr<GeometryParameterSpaceVerticle> ObjFileLoader::parseParameterSpaceVerticle(string line) {
	double coords[3] = { 0.0, 0.0, 0.0 }; // u[, v][, w]
	string::size_type sz;
	line = line.substr(3);
	try {
		coords[0] = stod(line, &sz);
		line = line.substr(sz);
	}
	catch (...) {
		return nullptr;
	}
	try { // v coordinate may be absent
		coords[1] = stod(line, &sz);
		line = line.substr(sz);
	}
	catch (...) {
		return shared_ptr<GeometryParameterSpaceVerticle>(new GeometryParameterSpaceVerticle(coords[0]));
	};
	try { // w coordinate may be absent
		coords[2] = stod(line, &sz);
	}
	catch (...) {
		return shared_ptr<GeometryParameterSpaceVerticle>(new GeometryParameterSpaceVerticle(coords[0], coords[1]));
	};

	return shared_ptr<GeometryParameterSpaceVerticle>(new GeometryParameterSpaceVerticle(coords[0], coords[1], coords[2]));
}


shared_ptr<GeometryPolygonalFace> ObjFileLoader::parsePolygonalFace(string line) {
	line = line.substr(2);
	istringstream stream(line);
	string elementStr;
	shared_ptr<GeometryPolygonalFace> pf(new GeometryPolygonalFace());

	double coords[3] = { 0.0, 0.0, 0.0 };
	while (stream >> elementStr) {
		string elementsProcessedStr = elementStr;
		replace(elementsProcessedStr.begin(), elementsProcessedStr.end(), '/', ' ');

		string::size_type sz;
		try { // at least vertex value must exist
			coords[0] = stod(elementsProcessedStr, &sz);
			elementsProcessedStr = elementsProcessedStr.substr(sz);
		}
		catch (...) {
			return nullptr;
		}
		try {
			coords[1] = stod(elementsProcessedStr, &sz);
			elementsProcessedStr = elementsProcessedStr.substr(sz);
		}
		catch (...) {
			pf->appendFaceElement(shared_ptr<VFaceElement>(new VFaceElement(coords[0])));
			continue;
		}
		try {
			coords[2] = stod(elementsProcessedStr, &sz);
		}
		catch (...) {
			if (elementStr.find("//") != string::npos)
				pf->appendFaceElement(shared_ptr<VNFaceElement>(new VNFaceElement(coords[0], coords[1])));
			else
				pf->appendFaceElement(shared_ptr<VTFaceElement>(new VTFaceElement(coords[0], coords[1])));
			continue;
		}
		pf->appendFaceElement(shared_ptr<VTNFaceElement>(new VTNFaceElement(coords[0], coords[1], coords[2])));
	}

	return pf;
}


shared_ptr<GeometryMtlElement> ObjFileLoader::parseMtlElement(string line) {
	// get mtl element name
	int paramStrLen = 7;
	if (line.length() > paramStrLen) {
		string name = line.substr(paramStrLen, line.length() - paramStrLen);
		return shared_ptr<GeometryMtlElement>(new GeometryMtlElement(name));
	}
	else { // usemtl must have 1 parameter
		return nullptr; 
	}
}


shared_ptr<GeometrySmooth> ObjFileLoader::parseSmooth(string line) {
	// get smooth shading value
	int paramStrLen = 2;
	if (line.length() > paramStrLen) {
		string value = line.substr(paramStrLen, line.length() - paramStrLen);
		return shared_ptr<GeometrySmooth>(new GeometrySmooth(value));
	}
	else { // usemtl must have 1 parameter
		return nullptr;
	}
}


