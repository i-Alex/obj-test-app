#include "GeometryManager.h"


GeometryManager* GeometryManager::_instance = 0;

GeometryManager* GeometryManager::getInstance() {
	return _instance;
}

void GeometryManager::init(shared_ptr<Geometry> geometry) {
	_instance = new GeometryManager(geometry);
}

shared_ptr<Geometry> GeometryManager::getGeometry() {
	return _geometry;
}
GeometryManager::GeometryManager(shared_ptr<Geometry> geometry) {
	_geometry = geometry;
};
