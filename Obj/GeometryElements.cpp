#include "GeometryElements.h"
#include <sstream>
#include <iomanip>

using namespace std;


/* GeometryVerticle methods definition */
string GeometryVerticle::toString() {
	ostringstream s;
	s << setprecision(10) << "v  " << _x << " " << _y << " " << _z;
	if (_w != 1.0)
		s << " " << _w;
	return s.str();
}


/* GeometryVerticleTexture methods definition */
string GeometryVerticleTexture::toString() {
	ostringstream s;
	s << setprecision(10) << "vt " << _u << " " << _v << " " << _w;
	//if (_w != 0.0000)
		//s << " " << _w;
	return s.str();
}


/* GeometryVertexNormal methods definition */
string GeometryVertexNormal::toString() {
	ostringstream s;
	s << setprecision(10) << "vn " << _x << " " << _y << " " << _z;
	return s.str();
}


/* GeometryParameterSpaceVerticle methods definition */
string GeometryParameterSpaceVerticle::toString() {
	ostringstream s;
	s << setprecision(10);
	if (_actualParamCount == 3)
		s << "vp " << _u << " " << _v << " " << _w;
	else if (_actualParamCount == 2)
		s << "vp " << _u << " " << _v;
	else
		s << "vp " << _u;
	return s.str();
}


/* GeometryMtlElement methods definition */
string GeometryMtlElement::toString() {
	return "usemtl " + _name;
}


/* GeometrySmooth methods definition */
string GeometrySmooth::toString() {
	return "s " + _value;
}


/* VFaceElement methods definition */
string VFaceElement::toString() {
	ostringstream s;
	s << _vertex;
	return s.str();
}


/* VTFaceElement methods definition */
string VTFaceElement::toString() {
	ostringstream s;
	s << _vertex << "/" << _texture;
	return s.str();
}


/* VNFaceElement methods definition */
string VNFaceElement::toString() {
	ostringstream s;
	s << _vertex << "//" << _normal;
	return s.str();
}


/* VTNFaceElement methods definition */
string VTNFaceElement::toString() {
	ostringstream s;
	s << _vertex << "/" << _texture << "/" << _normal;
	return s.str();
}


/* GeometryPolygonalFace methods definition */
void GeometryPolygonalFace::appendFaceElement(shared_ptr<FaceElement> element) {
	_elements.push_back(element);
}


string GeometryPolygonalFace::toString() {
	ostringstream s;
	s << "f";
	for (auto item : _elements)
		s << " " << item->toString();

	return s.str();
}


/* GeometryGroup methods definition */
void GeometryGroup::appendElement(shared_ptr<GeometryBaseElement> element) {
	_elements.push_back(element);
	if (dynamic_pointer_cast<GeometryVerticle>(element) != nullptr)
		_verticleNumber++;
	else if (dynamic_pointer_cast<GeometryVertexNormal>(element) != nullptr)
		_normalNumber++;
	else if (dynamic_pointer_cast<GeometryVerticleTexture>(element) != nullptr)
		_textureNumber++;
	else if (dynamic_pointer_cast<GeometryPolygonalFace>(element) != nullptr) {
		_faceNumber++;
	}
}


string GeometryGroup::toString() {
	ostringstream s;
	if (_isShown)
		s << "g " + _name;
	for (auto item : _elements)
		s << endl << item->toString();

	return s.str();
}


/* GeometryObject methods definition */
void GeometryObject::appendGroup(shared_ptr<GeometryGroup> group) {
	_groups.push_back(group);
}


string GeometryObject::toString() {
	ostringstream s;
	if (_isShown)
		s << "o " + _name;
	for (auto item : _groups)
		s << endl << item->toString();

	return s.str();
}