#include "Geometry.h"
#include <string>
#include <set>
#include <sstream>

using namespace std;


void Geometry::setMtllib(string libName) {
	_mtllib = libName;
}

void Geometry::appendObject(shared_ptr<GeometryObject> object) {
	_objects.push_back(object);
}

// not an optimal solution due to not optimal Geometry class structure, that was developed only for object parsing\transmitting.
shared_ptr<Geometry> Geometry::getPart(int faceStartIndex, int count) {
	vector<shared_ptr<GeometryPolygonalFace>> faceElements;
	vector<pair<int, shared_ptr<GeometryVerticle>>> verticles; // int for indexes;

	// get faces
	int currentIndex = faceStartIndex - 1; // because Obj indexation starts with 1, not with 0
	int endIndex = currentIndex + count; // index of last face.

	int currentFaceIndex = 0;
	for (auto obj : _objects) {
		for (auto group : obj->_groups) {
			if (group->_faceNumber + currentFaceIndex < currentIndex) { // skip faces
				currentFaceIndex += group->_faceNumber;
				continue;
			}

			for (auto element : group->_elements) {
				shared_ptr<GeometryPolygonalFace> face = dynamic_pointer_cast<GeometryPolygonalFace>(element);
				if (face != nullptr) {
					if (currentFaceIndex != currentIndex) {
						currentFaceIndex++;
						continue;
					}
					faceElements.push_back(face);
					currentIndex++;
					if (currentIndex == endIndex)
						break;
				}
			}
			if (currentIndex == endIndex)
				break;
		}
		if (currentIndex == endIndex)
			break;
	}

	// get vertex indexes
	set<int> verticleIndexes;
	for (auto face : faceElements)
	for (auto faceElem : face->_elements)
		verticleIndexes.insert(faceElem->getVertex());

	if (verticleIndexes.size() == 0)
		return nullptr;
	
	// get related verticles and save their indexes
	auto current = verticleIndexes.begin();
	auto end = verticleIndexes.end();
	int currentVerticleIndex = 0;
	int vi = *current - 1; // index of current verticle, starts with 0
	for (auto obj : _objects) {
		for (auto group : obj->_groups) {
			if (group->_verticleNumber + currentVerticleIndex < vi) { // skip faces
				currentVerticleIndex += group->_verticleNumber;
				continue;
			}

			for (auto element : group->_elements) {
				shared_ptr<GeometryVerticle> verticle = dynamic_pointer_cast<GeometryVerticle>(element);
				if (verticle != nullptr) {
					if (currentVerticleIndex != vi) {
						currentVerticleIndex++;
						continue;
					}
					verticles.push_back(pair<int, shared_ptr<GeometryVerticle>>(vi + 1, verticle));
					current++;
					if (current == end)
						break;
					vi = *current - 1;
				}
			}
			if (current == end)
				break;
		}
		if (current == end)
			break;
	}

	// reindexate face elements verticles and remove texture and normal indexes
	for (int i = 0; i < verticles.size(); i++) { // key is an old index, i+1 is a new one
		for (auto face : faceElements) {
			for (int j = 0; j < face->_elements.size(); j++)
			if (face->_elements[j]->_vertex == verticles[i].first)
				face->_elements[j] = shared_ptr<VFaceElement>(new VFaceElement(i + 1));
			else
				face->_elements[j] = shared_ptr<VFaceElement>(new VFaceElement(face->_elements[j]->_vertex));

		}
	}

	// fill geometry
	shared_ptr<GeometryGroup> group(new GeometryGroup());
	for (auto vert : verticles)
		group->appendElement(vert.second);
	for (auto face : faceElements)
		group->appendElement(face);
	shared_ptr<GeometryObject> object(new GeometryObject());
	object->appendGroup(group);
	shared_ptr<Geometry> g(new Geometry());
	g->appendObject(object);

	return g;
}

string Geometry::toString() {
	ostringstream s;
	if (_mtllib.length() > 0)
		s << "mtllib " + _mtllib;
	for (auto item : _objects)
		s << endl << item->toString();

	return s.str();
}