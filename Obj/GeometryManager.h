#ifndef HH_GEOMETRY_MANAGER_
#define HH_GEOMETRY_MANAGER_

#include <string>
#include "Geometry.h"

using namespace std;

class GeometryManager {
public:
	static GeometryManager* getInstance();
	static void init(shared_ptr<Geometry> geometry);

	shared_ptr<Geometry> getGeometry();

private:
	static GeometryManager* _instance;
	shared_ptr<Geometry> _geometry;

	GeometryManager(shared_ptr<Geometry> geometry);
};

#endif // HH_GEOMETRY_MANAGER_