#ifndef HH_OBJ_FILE_LOADER_
#define HH_OBJ_FILE_LOADER_

#include <string>
#include "Geometry.h"

using namespace std;

class ObjFileLoader {
public:
	static bool loadObj(shared_ptr<Geometry> geometry, string fileName);
	static bool loadObj(shared_ptr<Geometry> geometry, istream& objData);

private:
	ObjFileLoader() {}

	static shared_ptr<GeometryObject> parseObject(string line);
	static shared_ptr<GeometryGroup> parseGroup(string line);

	static shared_ptr<GeometryVerticle> parseVerticle(string line);
	static shared_ptr<GeometryVerticleTexture> parseVerticleTexture(string line);
	static shared_ptr<GeometryVertexNormal> parseVertexNormal(string line);
	static shared_ptr<GeometryParameterSpaceVerticle> parseParameterSpaceVerticle(string line);
	static shared_ptr<GeometryPolygonalFace> parsePolygonalFace(string line);
	static shared_ptr<GeometryMtlElement> parseMtlElement(string line);
	static shared_ptr<GeometrySmooth> parseSmooth(string line);
};

#endif // HH_OBJ_FILE_LOADER_