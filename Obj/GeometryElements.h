#ifndef HH_GEOMETRY_ELEMENTS_
#define HH_GEOMETRY_ELEMENTS_

#include <string>
#include <vector>
#include <memory>

using namespace std;

class GeometryBaseElement {
public:
	friend class Geometry;

	virtual string toString() = 0;
};


class GeometryVerticle : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryVerticle(double x, double y, double z, double w) : _x(x), _y(y), _z(z), _w(w) {};
	string toString() override;

private:
	double _x;
	double _y;
	double _z;
	double _w;
};


class GeometryVerticleTexture : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryVerticleTexture(double u, double v, double w) : _u(u), _v(v), _w(w) {};
	string toString() override;

private:
	double _u;
	double _v;
	double _w;
};


class GeometryVertexNormal : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryVertexNormal(double x, double y, double z) : _x(x), _y(y), _z(z) {};
	string toString() override;

private:
	double _x;
	double _y;
	double _z;
};


class GeometryParameterSpaceVerticle : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryParameterSpaceVerticle(double u, double v, double w) : _u(u), _v(v), _w(w), _actualParamCount(3) {};
	GeometryParameterSpaceVerticle(double u, double v) : _u(u), _v(v), _actualParamCount(2) {};
	GeometryParameterSpaceVerticle(double u) : _u(u), _actualParamCount(1) {};
	string toString() override;

private:
	double _u;
	double _v;
	double _w;
	
	int _actualParamCount;
};


class GeometryMtlElement : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryMtlElement(string name) : _name(name) {};
	string toString() override;

private:
	string _name;
};


class GeometrySmooth : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometrySmooth(string value) : _value(value) {};
	string toString() override;

private:
	string _value;
};


class FaceElement {
public:
	friend class Geometry;

	FaceElement(int vertex) : _vertex(vertex) {};
	virtual string toString() = 0;

	int getVertex() { return _vertex; }

protected:
	int _vertex;
};


class VFaceElement : public FaceElement {
public:
	friend class Geometry;

	VFaceElement(int vertex) : FaceElement(vertex) {};
	string toString() override;
};


class VTFaceElement : public FaceElement {
public:
	friend class Geometry;

	VTFaceElement(int vertex, int texture) : FaceElement(vertex), _texture(texture){};
	string toString() override;

private:
	int _texture;
};


class VNFaceElement : public FaceElement {
public:
	friend class Geometry;

	VNFaceElement(int vertex, int normal) : FaceElement(vertex), _normal(normal){};
	string toString() override;

private:
	int _normal;
};


class VTNFaceElement : public FaceElement {
public:
	friend class Geometry;

	VTNFaceElement(int vertex, int texture, int normal) : FaceElement(vertex), _texture(texture), _normal(normal){};
	string toString() override;

private:
	int _texture;
	int _normal;
};


class GeometryPolygonalFace : public GeometryBaseElement {
public:
	friend class Geometry;

	GeometryPolygonalFace() {};
	void appendFaceElement(shared_ptr<FaceElement> element);
	string toString() override;

private:
	vector<shared_ptr<FaceElement>> _elements;
};

class GeometryGroup {
public:
	friend class Geometry;

	GeometryGroup() : _isShown(false), _verticleNumber(0), _normalNumber(0), _textureNumber(0), _faceNumber(0) {};
	GeometryGroup(string name) : _name(name), _isShown(true), _verticleNumber(0), _normalNumber(0), _textureNumber(0), _faceNumber(0) {};
	void appendElement(shared_ptr<GeometryBaseElement> element);
	string toString();

private:
	string _name;
	bool _isShown;
	vector<shared_ptr<GeometryBaseElement>> _elements;
	int _verticleNumber;
	int _normalNumber;
	int _textureNumber;
	int _faceNumber;
};


class GeometryObject {
public:
	friend class Geometry;

	GeometryObject() : _isShown(false) {};
	GeometryObject(string name) : _name(name), _isShown(true) {};
	void appendGroup(shared_ptr<GeometryGroup> group);
	string toString();

private:
	string _name;
	bool _isShown;
	vector<shared_ptr<GeometryGroup>> _groups;
};

#endif // HH_GEOMETRY_ELEMENTS_