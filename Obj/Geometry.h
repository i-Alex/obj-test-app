#ifndef HH_GEOMETRY_
#define HH_GEOMETRY_

#include "GeometryElements.h"

using namespace std;

class Geometry {
public:
	Geometry() {};

	void setMtllib(string libName);
	void appendObject(shared_ptr<GeometryObject> object);

	shared_ptr<Geometry> getPart(int faceStartIndex, int count);
	string toString();

private:
	string _mtllib;
	vector<shared_ptr<GeometryObject>> _objects;
};

#endif // HH_GEOMETRY_