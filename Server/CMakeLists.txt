cmake_minimum_required(VERSION 3.4)

include_directories(${CMAKE_SOURCE_DIR}/Obj)
file(GLOB SOURCES *.cpp *.h)

add_executable(Server ${SOURCES})
target_link_libraries(Server Obj ws2_32)