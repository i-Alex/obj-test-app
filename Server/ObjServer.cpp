#include "ObjServer.h"
#include "GeometryManager.h"

#include <iostream>
#include <sstream>
#include <bitset>
//#include <fstream>

using namespace std;


bool ObjServer::init() {
	if (_isInit) {
		cout << "Server was already initialized!";
		return false;
	}
	WSADATA wsaData;
	int result;
	
	// init Winsock v2.2
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0) {
		cout << "WSAStartup failed with error:" << result << endl;
		return false;
	}

	struct addrinfo hints;
	struct addrinfo *resultAddr = nullptr;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// resolve the server address:port
	result = getaddrinfo(_host.c_str(), _port.c_str(), &hints, &resultAddr);
	if (result != 0) {
		cout << "Failed to get address info for host " << _host << " and port " << _port << ". Error " << result << "occurred." << endl;
		WSACleanup();
		return false;
	}

	// create server listen socket
	_listenSocket = INVALID_SOCKET;
	_listenSocket = socket(resultAddr->ai_family, resultAddr->ai_socktype, resultAddr->ai_protocol);
	if (_listenSocket == INVALID_SOCKET) {
		cout << "Socket failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(resultAddr);
		WSACleanup();
		return false;
	}

	// setup the TCP listening socket
	result = ::bind(_listenSocket, (sockaddr *)resultAddr->ai_addr, (int)resultAddr->ai_addrlen);
	if (result == SOCKET_ERROR) {
		cout << "Bind failed with error: " << WSAGetLastError() << endl;
		freeaddrinfo(resultAddr);
		closesocket(_listenSocket);
		WSACleanup();
		return false;
	}
	freeaddrinfo(resultAddr);

	result = listen(_listenSocket, SOMAXCONN);
	if (result == SOCKET_ERROR) {
		cout << "Listen failed with error: " << WSAGetLastError() << endl;
		closesocket(_listenSocket);
		WSACleanup();
		return false;
	}

	_isInit = true;
	return true;
}


void ObjServer::run() {
	if (!_isInit) {
		cout << "Init server first! Run failed.";
		return;
	}
	cout << "Server started on " << _host << ":" << _port << endl;

	sockaddr_in sinRemote;
	int nAddrSize = sizeof(sinRemote);

	while (true) {
		SOCKET clientSocket = INVALID_SOCKET;
		while ((clientSocket = accept(_listenSocket, (sockaddr*)&sinRemote, &nAddrSize)) == INVALID_SOCKET) {};

		cout << "Accepted connection from " << inet_ntoa(sinRemote.sin_addr) << ":" <<
			ntohs(sinRemote.sin_port) << "." << endl;

		DWORD nThreadID;
		CreateThread(0, 0, acceptClient, (void*)clientSocket, 0, &nThreadID);
	}
	//thread(acceptClient);

	while (true) {};
}

DWORD WINAPI acceptClient(CONST LPVOID socket) {
	SOCKET clientSocket = (SOCKET)socket;

	// process current connection
	char buffer[1000];
	int result;
	while (true) {
		string receivedCommand = "";
		do {
			result = recv(clientSocket, buffer, 1000, 0);
			if (result > 0) {
				buffer[result] = '\0';
				receivedCommand += buffer;
			}
		} while (result == 0);
		//buffer[0] = '\0';
		receivedCommand = from_binary(receivedCommand);
		cout << "Command received: " << receivedCommand << endl;

		// check command
		string answer;
		if (receivedCommand.compare("GET_OBJ") == 0)
			answer = GeometryManager::getInstance()->getGeometry()->toString();
		else if (receivedCommand.compare(0, 12, "GET_OBJ_PART") == 0) {
			string params = receivedCommand.substr(12);
			string::size_type sz;
			try {
				int start = stoi(params, &sz);
				int count = stoi(params.substr(sz));

				if (start < 1)
					answer = "Error: Start index is less than 1.";
				else if (count < 1)
					answer = "Error: Face count is less than 1.";
				else {
					shared_ptr<Geometry> g = GeometryManager::getInstance()->getGeometry()->getPart(start, count);
					answer = g->toString();
				}
			}
			catch (...) {
				answer = "Error: Can't parse 'GET_OBJ_PART' command. Incorrect syntax.";
			}
			
		}
		else if (receivedCommand.compare("CLOSE") == 0) {
			cout << "Connection closed." << endl;
			closesocket(clientSocket);
			return 0;
		}
		else
			answer = "Error: Command is wrong! Use 'GET_OBJ', 'GET_OBJ_PART' or 'CLOSE' instead.";

		const int blockLen = 1000;
		int blockCount = answer.length() / blockLen + 1;
		int i = 0;
		while (i < blockCount) {
			int currentBlockLen = answer.length() - i * blockLen;
			if (currentBlockLen > blockLen)
				currentBlockLen = blockLen;
			else if (currentBlockLen == 0)
				break;
			string block = to_binary(answer.substr(i * blockLen, currentBlockLen));
			result = send(clientSocket, block.c_str(), block.length(), 0);
			if (result == SOCKET_ERROR) {
				closesocket(clientSocket);
				return 0;
			}
			i++;
		}

		cout << "Data sent." << endl;
	}
	return 0;
}

string to_binary(const string str) {
	std::ostringstream oss;
	for (auto c : str) {
		oss << bitset<8>(c);
	}
	return oss.str();
}

string from_binary(const string binary) {
	stringstream ss(binary);
	string res = "";
	bitset<8> tmp;
	while (ss >> tmp)
		res += static_cast<char>(tmp.to_ulong());
	return res;
}