#include <iostream> 
#include "ObjFileLoader.h"
#include "ObjServer.h"
#include "GeometryManager.h"


using namespace std;

void main(int argc, char* argv[]) {
	// check parameters
	if (argc != 4) {
		cout << "Parameters number is wrong!" << endl 
			<< "Usage: Server.exe <hostname> <port> <obj file path>" << endl;
		return;
	}

	// load Obj file;
	shared_ptr<Geometry> g(new Geometry());
	GeometryManager::init(g);

	bool res = ObjFileLoader::loadObj(g, argv[3]);
	if (!res) {
		cout << "Can't load obj file '" << argv[3] << "'" << endl;
		return;
	}
	else
		cout << "Obj file loaded." << endl;

	// start server
	ObjServer *s = new ObjServer(argv[1], argv[2]);
	s->init();
	s->run();
}