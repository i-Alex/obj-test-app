#ifndef HH_OBJ_SERVER_
#define HH_OBJ_SERVER_

#include<winsock2.h>
#include <ws2tcpip.h>
#include <string>

using namespace std;


class ObjServer {
public:
	ObjServer(string host, string port) : _host(host), _port(port), _isInit(false) {};
	bool init();
	void run();
	
private:
	string _host;
	string _port;
	SOCKET _listenSocket;

	bool _isInit;
	
};

DWORD WINAPI acceptClient(CONST LPVOID socket);
string to_binary(const string str);
string from_binary(const string binary);
#endif // HH_OBJ_SERVER_